import React, { Component } from "react";
import {
        View,
        Text,
        KeyboardAvoidingView,
        ScrollView,
        StyleSheet
      } from "react-native";

import styles from "./loginstyle"
import colors from "../../../styles/color/colors";
import InputField from "../../../components/forms/inputfield";
import NextArrowButton from "../../../components/buttons/NextArrowButton"

export default class Login extends Component {
  render() {

    return (
      <KeyboardAvoidingView style={styles.wrapper} behavior="padding">
        <View style={styles.scrollViewWrapper}>
        <ScrollView style={styles.scrollView}>
          <Text style={styles.loginHeader}>Login</Text>
          <InputField
            labelText="EMAIL ADDRESS"
            labelTextSize={14}
            labelColor={colors.white}
            textColor={colors.white}
            borderBottomColor={colors.white}
            inputType="email"
            customStyle={styles.emailInput}

          />
          <InputField
            labelText="PASSWORD"
            labelTextSize={14}
            labelColor={colors.white}
            textColor={colors.white}
            borderBottomColor={colors.white}
            inputType="password"
            customStyle={styles.passwordInput}

          />
        </ScrollView>
         <NextArrowButton />
         </View>
       </KeyboardAvoidingView>
    );
  }
}
