
import { StyleSheet } from 'react-native';
import colors from "../../../styles/color/colors";

export default styles = StyleSheet.create({
  wrapper: {
    display: "flex",
    flex: 1,
    backgroundColor: colors.green01
  },
  scrollViewWrapper: {
    marginTop: 70,
    flex: 1
  },
  avoidView: {
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 20,
    flex:1
   },
  loginHeader: {
    fontSize: 28,
    color: colors.white,
    fontWeight: "300",
    marginBottom: 40,
    paddingLeft : 40,
  },
  emailInput :{
    paddingLeft : 40,
    paddingRight : 40,
    paddingTop : 20
  },
  passwordInput :{
    paddingLeft : 40,
    paddingRight : 40,
    paddingTop : 30
  }
});
