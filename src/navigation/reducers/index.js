import { combineReducers } from "redux";
import eventData from "features/events/reducers";
import exploreData from "features/explore/reducers";
import navigationData from "navigation/reducers";

export default combineReducers({
  eventData,
  exploreData,
  navigationData
});
