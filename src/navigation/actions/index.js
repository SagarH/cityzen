import { NavigationActions } from "react-navigation";
import * as screenNames from "../screen_names";

export const navigateToLogin = () =>
  NavigationActions.navigate({
    routeName: screenNames.LOGIN
  });

export const navigateToSplash = () =>
  NavigationActions.navigate({
    routeName: screenNames.SPLASH
  });
