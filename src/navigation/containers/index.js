import { StackNavigator } from "react-navigation";
import AuthNavigator from "navigators/authnavigator";
import MainNavigator from "navigators/mainnavigator";
import * as screenNames from "../screen_names";
import Splash from "features/splash/containers";

const appNavigator = StackNavigator({
  [screenNames.SPLASH]: {
    screen: Splash
  },

  [screenNames.LOGIN]: {
    screen: AuthNavigator
  },

  [screenNames.MAIN]: {
    screen: MainNavigator
  }
});

export default appNavigator;
