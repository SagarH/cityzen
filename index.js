/**
 * @format
 */
import React, { Component } from "react";
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import MainNavigator from './src/navigation/navigators/mainnavigator';
AppRegistry.registerComponent(appName, () => App);

export default class LargeApp extends React.Component {
  render() {
    return (
      //<Provider store={}>
        <Layout>
          <MainNavigator />
        </Layout>
      //</Provider>
    );
  }
}
// export default class LargeApp extends Component {
//   render() {
//     return (
//       <Provider store={myStore}>
//         <Layout>
//           <ApplicationNavigator />
//         </Layout>
//       </Provider>
//     );
//   }
// }
